<?php

function generate_menu()
{

	$ci 	=& get_instance();
	if($ci->session->has_userdata('header_menu'))
	{
		print $ci->session->userdata('header_menu');

	}
	else
	{
		$res 	= '';
		$ci->db->from('menu_access a');
		$ci->db->join('menu b', 'a.menu_id = b.id', 'left');
		$ci->db->where('b.parent_code', '');
		$ci->db->where_in('a.group_id', $_SESSION['groups'] );
		$ci->db->group_by("a.menu_id");
		$parent = $ci->db->get()->result();
		// $parent = $ci->db->get_where('menu', array('parent_code' => ''))->result();
		foreach ($parent as $value) 
		{
			//check parent if have child
			$check_sub 		= is_have_submenu($value->id);
			$is_dropdown 	= (!$check_sub) ? '' : 'dropdown-toggle';  

			$res .= '
			<li class="hover">
				<a href="'.site_url($value->link).'" class="'.$is_dropdown.'">
					<i class="menu-icon fa fa-'.$value->icon.'"></i>
					<span class="menu-text"> '.$value->name.' </span>
					<b class="arrow fa fa-angle-down"></b>
				</a>
				
				<b class="arrow"></b>';
			$res .= submenu($value->id);

		} 

		$res .='</li>'; 
		$ci->session->set_userdata('header_menu', $res);
		print $res;
	}

}


function is_have_submenu($id)
{
	$ci 	=& get_instance();
	$ci->db->from('menu_access a');
	$ci->db->join('menu b', 'a.menu_id = b.id', 'left');
	$ci->db->where('b.parent_code', $id);
	$ci->db->where_in('a.group_id', $_SESSION['groups'] );
	$ci->db->group_by("a.menu_id");
	$sub = $ci->db->get()->result();
	return $result = count($sub) > 0 ? true : false;
}

function submenu($id)
{
	$ci 	=& get_instance();
	$ci->db->from('menu_access a');
	$ci->db->join('menu b', 'a.menu_id = b.id', 'left');
	$ci->db->where('b.parent_code', $id);
	$ci->db->where_in('a.group_id', $_SESSION['groups'] );
	$ci->db->group_by("a.menu_id");
	$sub = $ci->db->get()->result();
	// $sub 	= $ci->db->get_where('menu', array('parent_code'=>$id) )->result();
	$part 	= '';
	if(count($sub) > 0)
	{
		$part .= '<ul class="submenu can-scroll">';
		foreach ($sub as $sm) 
		{
			
			$part .= '
					<li class="hover">
						<a href="'.site_url($sm->link).'">
							<i class="menu-icon fa fa-caret-right"></i>
							<span class="menu-text"> '.$sm->name.' </span>
						</a>
						
						<b class="arrow"></b>';
			$part .= submenu($sm->id);
		}

		$part .= '</li></ul>';
	}
	
	return $part;

}

function is_permit($action, $code)
{
	$ci 		=& get_instance();
	$permission = $ci->session->userdata('permission');
	$list 		= array('create' => 'c', 'read' => 'r', 'update' => 'u', 'delete' => 'd');
	$result 	= array();
	if(!empty($permission))
	{
		foreach ($permission as $value) 
		{
			$val = explode('|=|=|', $value);
			if($code == $val[1])
			{
				if(strpos($val[0], $list[$action]) !== false)
				{
					$result[] = 'true';
				}
				else
				{
					$result[] = 'false';
				}
				
			}
			else
			{
				$result[] = 'false'; 
			}


		}

		if(in_array('true', $result))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function validate_input($data) 
{
  $res = trim($data);
  $res = stripslashes($res);
  $res = htmlspecialchars($res);
  return $res;
}

function generate_log() 
{
	$ci 		=& get_instance();
    
    $ip         = '127.0.0.1';
    $url_act    = 'foward';
    $ip         = getip();        
    $path       = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];   

    $get   		= json_encode($_GET); 
    $post  		= json_encode($_POST);
    $body  		= file_get_contents("php://input");
    $field 		= array(
    	'id'			=> null,
        'local_time'    => date('Y-m-d H:i:s'),
        'from_ip'       => $ip,
        'url_landing'   => $path,
        'url_post'      => $post,
        'url_get'       => $get,
        'url_body'      => $body,
        'action'        => $url_act
    );

    $ci->db->insert('web_traffic', $field);

}

function getip() 
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))  { $ip = $_SERVER['HTTP_CLIENT_IP'];  
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
    } else {  
        $ip = $_SERVER['REMOTE_ADDR'];  
    }  
    return $ip; 
}

function base64url_encode($data) 
{ 
   return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

function base64url_decode($data) 
{ 
   return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
} 