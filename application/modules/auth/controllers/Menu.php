<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation', 'encryption'));
		$this->load->helper(array('language', 'general'));

		$this->load->model('datatables_model','datatables');
		$this->load->model('general_model','general');
        $this->load->model(array('role_model', 'auth_model') );

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if (!$this->ion_auth->logged_in())
		{
            // redirect them to the login page
			redirect('login-is-required', 'refresh');
		}
	}


	public function index()
	{
		if(is_permit('read', 'menu'))
		{
			$this->data['csrf'] 	= $this->general->_get_csrf_nonce();
            $this->template
            ->set_partial('js', 'js_menu')
            ->build('menu', $this->data);
    	}
    	else
    	{
    		redirect('auth/logout','refresh');
    	}	
		
	}

	//ajax list group
    public function ajax_list_menu()
    {
		$table 			= 'menu';
		$column_order 	= array(null, 'name', 'description', 'icon', 'link', 'parent_code'); 
		$column_search 	= array('name', 'description', 'icon', 'link', 'parent_code');  
		$order 			= array('id' => 'asc'); // default order 
       
        $list = $this->datatables->get_datatables($table, $column_order, $column_search, $order);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $menu_model) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $menu_model->name;
            $row[] = $menu_model->description;
            $row[] = $menu_model->icon;
            $row[] = $menu_model->link;
            $row[] = (!empty($menu_model->parent_code)) ? $this->general->get_by_id('menu', $menu_model->parent_code)->name : '';
            if(is_permit('update', 'user') || is_permit('delete', 'user') )
			{
				if(is_permit('update', 'user'))
				{
					$update = '<a class="green" href="#" onclick="edit_menu(\''.$menu_model->id.'\')">
								<i class="ace-icon fa fa-pencil bigger-130"></i>
							</a>';
				}
				
				if(is_permit('delete', 'user'))
				{
					$delete = '<a class="red" href="#" onclick="delete_menu(\''.$menu_model->id.'\')">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>
							</a>';
				}

	            $row[] = '	<div class="hidden-sm hidden-xs action-buttons">
								'.$update.$delete.'
							</div>';
			}

            

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatables->count_all(),
                        "recordsFiltered" => $this->datatables->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function dropdown_menu()
    {
    	if($this->input->post('group_id'))
    	{
    		$edit  	= $this->input->post('edit_id'); 
    		$id 	= base64url_decode($this->input->post('group_id') ); 
    		$menu 	= $this->auth_model->get_menu_byGroup($id, $edit);
    	}
    	else
    	{
    		$menu = $this->general->get_all('menu');
    	}

    	$edit_id = $this->input->post('edit_id');
    	$edit_id = empty($edit_id) ? '' : $edit_id;


		$list = '<option value="">select parent code..</option>';
		if(!empty($menu))
		{
			foreach ($menu as $value) 
			{
				if(!empty($value->id) && $value->id != $edit_id)
				{
					$list .= '<option value="'.$value->id.'"> '.$value->name.' </option>';
				}
			}
		}

		echo $list;
    }

    public function ajax_add_menu()
	{
		$this->_validate();
		$data 	= array(
				'name' => $this->input->post('name'),
				'code' => strtolower(str_replace(array(' '), '_', $this->input->post('name') ) ),
				'description' => $this->input->post('description'),
				'icon' => $this->input->post('icon'),
				'link' => $this->input->post('link'),
				'parent_code' => $this->input->post('parent_code'),
			);

		$insert = $this->general->save('menu', $data);
		echo json_encode(array("status" => TRUE));
	}
	
	//ajax edit menu
	public function ajax_edit_menu($id)
	{
		$data = $this->general->get_by_id('menu', $id);
		echo json_encode($data);
	}

	//ajax update group
	public function ajax_update_menu()
	{
		$this->_validate();
		$data 	= array(
				'name' => $this->input->post('name'),
				'code' => strtolower(str_replace(array(' '), '_', $this->input->post('name') ) ),
				'description' => $this->input->post('description'),
				'icon' => $this->input->post('icon'),
				'link' => $this->input->post('link'),
				'parent_code' => $this->input->post('parent_code'),
			);

		$this->general->update('menu', array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}
	//ajax delete group
	public function ajax_delete_menu($id)
	{
		$this->db->set('parent_code', '');
		$this->db->where('parent_code', $id);
		if($this->db->update('menu') )
		{
			$this->auth_model->delete_Menuby_id($id);
		}
		
		echo json_encode(array("status" => TRUE));
	}  

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}  

}

/* End of file Menu.php */
/* Location: ./application/modules/auth/controllers/Menu.php */