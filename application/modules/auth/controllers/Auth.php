<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation', 'encryption'));
		$this->load->helper(array('language', 'general'));

		$this->load->model('datatables_model','datatables');
		$this->load->model('general_model','general');
        $this->load->model(array('role_model', 'auth_model') );

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	// redirect if needed, otherwise display the user list
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
            // redirect them to the login page
			redirect('login-is-required', 'refresh');
		}
		
		else
		{
			$this->template
	        ->build('dashboard');
		}
	}

	public function get_events()
	{
		$area = $this->input->get('area_id');
		$room = $this->input->get('room_id');
		// var_dump($_GET);exit;
		if(!empty($area) && !empty($room))
		{
			$data = $this->db->get_where('event', array('area_id' => $area, 'room_id' => $room) )->result();
		}
		else
		{
			$data = array();
			// $data = $this->db->get('event')->result();
		}

		echo json_encode($data);
	}

	public function get_rooms()
	{
		$area = $this->input->post('area');
		$data = $this->db->get_where('room', array('area_id' => $area) )->result();
		$resp = '';
		foreach ($data as $value) 
		{
			$resp .= '<option value="'.$value->id.'">'.$value->room_name.' - floor '.$value->floor.'</option>';
		}

		echo $resp;

	}

	public function ajax_edit_event($id)
	{
		$data = $this->general->get_by_id('event', $id);
		$data->event_date = $data->start.' - '.$data->end;
		$data->start_date = date("m/d/Y", strtotime($data->start));
		$data->end_date = date("m/d/Y", strtotime($data->end));
		echo json_encode($data);
	}

	// log the user in
	public function login()
	{
		if($this->ion_auth->logged_in())
		{
			redirect('root', 'refresh');
			exit;
		}
		
		$this->data['title'] = $this->lang->line('login_heading');

		//validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');


		if ($this->form_validation->run() == true && $this->general->_valid_csrf_nonce() === true)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');
			$identity = validate_input($this->input->post('identity') );
			$password = validate_input($this->input->post('password') );

			if ($this->ion_auth->login($identity, $password, $remember))
			{
				//if login success redirect to the home page
				$this->auth_model->generate_permission($this->session->userdata('groups') );
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('root', 'refresh');
			}
			else
			{
				// if login failed redirect to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'class'	=> 'form-control',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
				'class'	=> 'form-control',

			);

			$this->data['csrf'] 	= $this->general->_get_csrf_nonce();
			$this->data['areas'] 	= $this->db->get('area')->result();
            $this->template
                    ->set_partial('js', 'js_login')
                    ->build('login', $this->data);
		}
	}

	// log the user out
	public function logout()
	{
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('login-is-required', 'refresh');
	}

	
	public function change_password()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('login-is-required', 'refresh');
		}
		else
		{
			$this->data['csrf'] 	= $this->general->_get_csrf_nonce();
	        $this->template
	        	 ->set_partial('js', 'js_change_password')
	             ->build('change_password', $this->data);
	    }
        	
	}

	public function reset_password()
	{

		$email 		= $this->input->post('email');
		$is_exist 	= $this->db->get_where('users', array('email' => $email) )->row(); 
		if(!empty($is_exist))
		{
			$new_pass 	= microtime().rand(5, 15);
			$new_pass 	= base64_encode($new_pass);
			$data 		= array('password' => $new_pass);
			$this->ion_auth->update($is_exist->id, $data);
			$this->load->library('email');
			$config = array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'tls://smtp.gmail.com',
				  'smtp_port' => 465,
				  'smtp_user' => 'e.meetingroom@gmail.com',
				  'smtp_pass' => 'e-meeting123',
				);

			$this->email->initialize($config);
			$this->email->from('hrm@email.com', 'Admin');
			$this->email->to($is_exist->email);
			
			$this->email->subject('RESET PASSWORD');
			$this->email->message('you have requested for new password, please use : '.$new_pass.' as your new password');
			
			if($this->email->send())
			{
				echo "password reset succesfully, please check your email. <a href='".site_url('auth')."'>Click here</a> to login..";
			}
			else
			{
				echo "password reset failed, please <a href='".site_url('auth')."'>click here</a> and retry..";
			}
			
		}		
	}   

	


}
