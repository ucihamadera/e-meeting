
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				<a href="<?=site_url('auth/group')?>">Groups</a>
			</small>
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Menu Group Management
			</small>
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			 <h3>Menu Data</h3>
				<br>
        		<button class="btn btn-success" onclick="add_menu()"><i class="glyphicon glyphicon-plus"></i> Add Menu</button>
				<hr>
				<div class="table-header">
					Group Name : <?=$group_name;?>
				</div>

				<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
						
							<th>No</th>
							<th>Name</th>
							<th>Description</th>
							<th>Access</th>
							<th>-</th>
						
						</tr>
					</thead>
					<tbody>
					</tbody>

					<tfoot>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Description</th>
							<th>Access</th>
							<th>-</th>

						</tr>
					</tfoot>
				</table>
  				
  				<!-- form input/edit -->
				<hr>

				<!-- Bootstrap modal -->
				<div class="modal fade" id="modal_form" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                <h3 class="modal-title">Menu Form</h3>
				            </div>
				            <div class="modal-body form">
				            	<?=form_open('#', array('id'=>'form', 'class'=>'form-horizontal') ); ?>
				                	<?=form_hidden($csrf); ?>
				                    <input type="hidden" value="" name="id"/> 
				                    <div class="form-body">
				                        <div id="msg" class="alert alert-danger">
				                        	
				                        </div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Menu</label>
											<div class="col-md-9" id="parent_area">

												<select name="menu_id" required class="select2-js form-control" style="width: 100%">
														
												</select>

											</div>
										</div>
										
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Access</label>
											<div class="col-md-9 box-access">
											<div class="checkbox">
												
													<label>
														<input value="c" name="access[]" class="ace" type="checkbox">
														<span class="lbl"> create</span>
													</label>
											</div>
											<div class="checkbox">
												
													<label>
														<input value="r" name="access[]" class="ace" type="checkbox">
														<span class="lbl"> read</span>
													</label>
											</div>
											<div class="checkbox">
												
													<label>
														<input value="u" name="access[]" class="ace" type="checkbox">
														<span class="lbl"> update</span>
													</label>
											</div>
											<div class="checkbox">
												
													<label>
														<input value="d" name="access[]" class="ace" type="checkbox">
														<span class="lbl"> delete</span>
													</label>
											</div>
											
											</div>
										</div>
										
										
				                    </div>
				                <?=form_close();?>
				            </div>
				            <div class="modal-footer">
				                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				            </div>
				        </div><!-- /.modal-content -->
				    </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<!-- End Bootstrap modal -->
  				

			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

  
       