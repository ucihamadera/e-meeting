<div class="page-content" style="background:#FFFFF;">
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <!-- PAGE CONTENT BEGINS -->
      <div class="widget-box" id="login-box" style="display:none;">
        <div class="widget-header">
          <h4>Login</h4>
        </div>
        <div class="widget-body">
          <div class="widget-main">
            <div id="infoMessage"><?php echo $this->session->flashdata('message');?></div>
            <?php echo form_open("login-is-required", array('id'=>'f_login', 'class'=>'form-horizontal'));?>
            <?=form_hidden($csrf); ?>
            <div class="form-group">
              <label class="col-xs-6 col-md-2" for="form-field-1"> Username </label>
              <div class="col-xs-8 col-md-4">
                <?php echo form_input($identity);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-6 col-md-2" for="form-field-1"> Password </label>
              <div class="col-xs-8 col-md-4">
                <?php echo form_input($password);?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-6 col-md-2">
                <?php echo lang('login_remember_label', 'remember');?>
              </div>
              <div class="col-xs-6 col-md-2">
                <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-md-12 pull-right">
                <button type="submit" class="btn btn-default">Login</button>
              </div>
            </div>
            <?php echo form_close();?>
            <p><a href="javascript:void(0)" onclick="reset_pass()"><?php echo lang('login_forgot_password');?></a></p>
            <?php echo form_open("auth/reset_password", array('id'=>'f_reset', 'class'=>'form-horizontal'));?>
            <div class="form-group">
              <label class="col-xs-6 col-md-2" for="form-field-1"> Email Address </label>
              <div class="col-xs-8 col-md-3">
                <input type="text" name="email" class="form-control" value="" required="required">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-warning">Reset Password</button>
                <button type="button" onclick="cancel_reset()" class="btn btn-info">Cancel</button>
              </div>
            </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>


      <div class="panel panel-default">
        <div class="panel-heading">
          <form id="form-filter" class="form-horizontal">
            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Area Name</label>
              <div class="col-sm-8">
                <select name="area_id" class="form-control">
                  <option value="">--select area--</option>
                  <?php 
                  foreach ($areas as $area) 
                  {
                    echo '<option value="'.$area->id.'"> '.$area->name.' </option>';
                  }

                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Room Name</label>
              <div class="col-sm-8">
                <select name="room_id" class="form-control">
                  <option value="">--select room--</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="LastName" class="col-sm-2 control-label"></label>
              <div class="col-sm-8">
                <button type="submit" id="btn-filter" class="btn btn-primary">Filter</button>
                <button type="button" onclick="show_login()" class="btn btn-success pull-right">Form Login</button>
              </div>
            </div>
          </form>
        </div>
        <div class="panel-body">
          <div class="space"></div>
          <div id="calendar"></div>
        </div>
      </div>

      <!-- Bootstrap modal -->

      <div class="modal fade" id="modal-form">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <form action="" class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-3 control-label">Area</label>
                <div class="col-sm-9">
                  <span class="form-control area_id"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Room</label>
                <div class="col-sm-9">
                  <span class="form-control room_id"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                  <span class="form-control title"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Date Time</label>
                <div class="col-sm-9">
                  <span class="form-control event_date"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Desc</label>
                <div class="col-sm-9">
                  <span class="form-control desc"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Qty</label>
                <div class="col-sm-9">
                  <span class="form-control qty"></span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Notes</label>
                <div class="col-sm-9">
                  <span class="form-control notes"></span>
                </div>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Bootstrap modal -->




    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->