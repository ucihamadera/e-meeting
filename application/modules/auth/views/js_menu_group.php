
<script type="text/javascript">

var save_method; //for save method string
var table;
var base_url = '<?php echo base_url();?>';
var uri;
$(document).ready(function() {
    
    
    $('.select2-js').select2({placeholder: 'please select..', allowClear: true}); 
    //datatables
    uri = "<?=$id?>";
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('auth/group/ajax_list_menugroup')?>",
            "type": "POST",
            "data" : {group_id:uri}
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

f
     //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    // $("select").change(function(){
    //     $(this).parent().parent().removeClass('has-error');
    //     $(this).next().empty();
    // });

});


function add_menu()
{
    save_method = 'add';
    $('#msg').hide();
    $('#form')[0].reset(); // reset form on modals
    $('.form-menu').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add menu access'); // Set Title to Bootstrap modal title
    reload_menu_list();

}

function edit_menu(id)
{
    reload_menu_list(id);
    save_method = 'update';
    $('#msg').hide();
    $('#form')[0].reset(); // reset form on modals
    $('.form-menu').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string


    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('auth/group/ajax_edit_menugroup')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            setTimeout(function(){
                var values = data.crud;
                if(values != '')
                {
                    $(".box-access").find('[value=' + values.join('], [value=') + ']').prop("checked", true);
                }

                $('[name="menu_id"]').val(data.menu_id);
                $('[name="menu_id"]').select2().trigger('change');
                $('[name="id"]').val(data.id);

            }, 1000); 
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit menu'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function reload_menu_list(id)
{
    if(id == '')
    {
        var id = '';
    }

    uri = "<?=$id?>";
   
    $('[name="menu_id"]').select2('destroy').empty();
    $.post("<?php echo site_url('auth/menu/dropdown_menu')?>", {group_id: uri, edit_id:id }, function(resp, textStatus, xhr) {
        /*optional stuff to do after success */
        $('[name="menu_id"]').append(resp);
    });
    

    $('[name="menu_id"]').val('');
    
    $('[name="menu_id"]').select2({placeholder:'select menu..'}).trigger('change');
    
}

function save()
{
    uri = "<?=$id?>";
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    if(save_method == 'add') {
        url = "<?php echo site_url('auth/group/ajax_add_menugroup')?>";
    } else {
        url = "<?php echo site_url('auth/group/ajax_update_menugroup')?>";
    }

    // ajax adding data to database

    var formData = new FormData($('#form')[0]);
    formData.append('uri', uri);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                $('#msg').show();
                $('#msg').html('');
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#msg').append(data.error_string[i]+'<br>');
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_menu(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('auth/group/ajax_delete_menugroup')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}


</script>
