<script>
	var save_method = 'update';
	var base_url = '<?php echo base_url();?>';

	$(function() {
		edit_user(); 
	});
	
	function edit_user()
	{
	    // save_method = 'update';
	    $('#form')[0].reset(); // reset form on modals
	    $('#msg').html('');
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string


	    //Ajax Load data from ajax
	    $.ajax({
	        url : "<?php echo site_url('auth/user/ajax_edit_user')?>/",
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {

	            $('[name="id"]').val(data.id);
	            $('[name="first_name"]').val(data.first_name);
	            $('[name="last_name"]').val(data.last_name);
	            $('[name="username"]').val(data.username);
	            $('[name="password"]').next().html('<i>input password to change old password</i>');
	            $('[name="phone"]').val(data.phone);
	            $('[name="email"]').val(data.email);
	            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title

	            $('#photo-preview').show(); // show photo preview modal

	            if(data.photo)
	            {
	                $('#label-photo').text('Change Photo'); // label photo upload
	                $('#photo-preview div').html('<img src="'+base_url+'upload/user/'+data.photo+'" class="img-responsive" style="height:100px" >'); // show photo
	                $('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.photo+'"/> Remove photo when saving'); // remove photo

	            }
	            else
	            {
	                $('#label-photo').text('Upload Photo'); // label photo upload
	                $('#photo-preview div').text('(No photo)');
	            }


	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
	}


	function save()
	{
	    $('#btnSave').text('saving...'); //change button text
	    $('#btnSave').attr('disabled',true); //set button disable 
	    var url;

	    if(save_method == 'add') {
	        url = "<?php echo site_url('auth/user/ajax_add_user')?>";
	    } else {
	        url = "<?php echo site_url('auth/user/ajax_update_user')?>";
	    }

	    // ajax adding data to database

	    var formData = new FormData($('#form')[0]);
	    $.ajax({
	        url : url,
	        type: "POST",
	        data: formData,
	        contentType: false,
	        processData: false,
	        dataType: "JSON",
	        success: function(data)
	        {

	            if(data.status) //if success close modal and reload ajax table
	            {
	                $('#msg').html('Whoooo, Data has been saved..<hr>');
	                setTimeout(function() {
				  		edit_user();
					}, 1500);
	            }
	            else
	            {
	            	if(typeof data.msg !== 'undefined')
	            	{
	                	$('#msg').html(data.msg+'<hr>');
	            	}

	                if(typeof data.inputerror !== 'undefined')
	                {

	                    for (var i = 0; i < data.inputerror.length; i++) 
	                    {
	                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
	                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
	                    }
	                }
	            }
	            $('#btnSave').text('save'); //change button text
	            $('#btnSave').attr('disabled',false); //set button enable 

	            setTimeout(function() {
				  edit_user();
				}, 1500);


	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	            $('#btnSave').text('save'); //change button text
	            $('#btnSave').attr('disabled',false); //set button enable 

	        }
	    });
	}
</script>