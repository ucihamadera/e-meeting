
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				User Management
			</small>
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			 <h3>Users Data</h3>
				<br>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title" >Custom Filter : </h3>
					</div>
					<div class="panel-body">
						<form id="form-filter" class="form-horizontal">
							
							<div class="form-group">
								<label for="username" class="col-sm-2 control-label">User Name</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="username">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="email">
								</div>
							</div>
							<div class="form-group">
								<label for="first_name" class="col-sm-2 control-label">First Name</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="first_name">
								</div>
							</div>
							<div class="form-group">
								<label for="last_name" class="col-sm-2 control-label">Last Name</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="last_name">
								</div>
							</div>
							
							<div class="form-group">
								<label for="LastName" class="col-sm-2 control-label">Phone</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="phone">
								</div>
							</div>
							<div class="form-group">
								<label for="LastName" class="col-sm-2 control-label"></label>
								<div class="col-sm-4">
									<button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
									<button type="button" id="btn-reset" class="btn btn-default">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<?php if(is_permit('create', 'user') ) { ?>
        		<button class="btn btn-success" onclick="add_user()"><i class="glyphicon glyphicon-plus"></i> Add Person</button>
				<?php } ?>
				<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
						
							<th>No</th>
							<th>Username</th>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Photo</th>
							<?php if(is_permit('update', 'user') || is_permit('delete', 'user')) { ?>
							<th>-</th>
							<?php } ?>
						
						</tr>
					</thead>
					<tbody>
					</tbody>

					<tfoot>
						<tr>
							<th>No</th>
							<th>Username</th>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Photo</th>
							<?php if(is_permit('update', 'user') || is_permit('delete', 'user')) { ?>
							<th>-</th>
							<?php } ?>

						</tr>
					</tfoot>
				</table>
  				
  				<!-- form input/edit -->
				<hr>

				<!-- Bootstrap modal -->
				<div class="modal fade" id="modal_form" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                <h3 class="modal-title">User Form</h3>
				            </div>
				            <div class="modal-body form">
				            	<div id="msg" class="text-danger text-center"></div>

				            	<?=form_open('#', array('id'=>'form', 'class'=>'form-horizontal') ); ?>
				                	<?=form_hidden($csrf); ?>
				                    <input type="hidden" value="" name="id"/> 
				                    <div class="form-body">
				                        <div class="form-group">
											<label for="username" class="col-md-3 control-label">User Name</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="username">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Password</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="password">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="email" class="col-md-3 control-label">Email</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="email">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="first_name" class="col-md-3 control-label">First Name</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="first_name">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="last_name" class="col-md-3 control-label">Last Name</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="last_name">
												<span class="help-block"></span>
											</div>
										</div>
										
										<div class="form-group">
											<label for="LastName" class="col-md-3 control-label">Phone</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="phone">
												<span class="help-block"></span>
											</div>
										</div>
				                        <div class="form-group" id="photo-preview">
				                            <label class="control-label col-md-3">Photo</label>
				                            <div class="col-md-9">
				                                (No photo)
				                                <span class="help-block"></span>
				                            </div>
				                        </div>
				                        <div class="form-group">
				                            <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
				                            <div class="col-md-9">
				                                <input name="photo" type="file">
				                                <span class="help-block"></span>
				                            </div>
				                        </div>
				                    </div>
				                <?=form_close();?>
				            </div>
				            <div class="modal-footer">
				                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				            </div>
				        </div><!-- /.modal-content -->
				    </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<!-- End Bootstrap modal -->
  				

			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

  
       