
<script type="text/javascript">

var save_method; //for save method string
var table;
var base_url = '<?php echo base_url();?>';
var uri;

$(document).ready(function() {
    //datatables
    uri = "<?=$id?>";
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('auth/group/ajax_list_usergroup')?>",
            "type": "POST",
            "data": {uri:uri}
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

    

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save(id)
{
    uri = "<?=$id?>";

    var list_id = [];
    $(".data-check:checked").each(function() {
            list_id.push(this.value);
    });
    
    var url = "<?php echo site_url('auth/group/ajax_update_usergroup')?>";

    // ajax adding data to database

    $.ajax({
        url : url,
        type: "POST",
        data: {id:list_id, uri:uri},
        dataType: "JSON",
        success: function(data)
        {
            if(data.status)
            {
                reload_table();
            }
            else
            {
                alert('Failed.');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}




</script>
