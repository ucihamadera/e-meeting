
<script type="text/javascript">

var save_method; //for save method string
var table;
var base_url = '<?php echo base_url();?>';

$(document).ready(function() {
    // daterange
    $('.input-daterange').daterangepicker({
            'autoUpdateInput': false,
            'applyClass' : 'btn-sm btn-success',
            'cancelClass' : 'btn-sm btn-default',
            locale: {
                applyLabel  : 'Apply',
                cancelLabel : 'Cancel',
            }
        })

    $('.input-daterange').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    });

    $('.input-daterange').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    //datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('report/ajax_event_request')?>",
            "type": "POST",
            "data": function ( data ) {
                data.event_date   = $('#event_date').val();
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
        dom: 'Bflrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],

    });

    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });


});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function approve_event(id)
{
    $('#msg').addClass('hidden');
    $.get("<?=site_url('report/approve_event')?>/"+id, function(data) {
        reload_table();
        $('#msg').removeClass('hidden').html('event has been approved..');
    });
}

</script>
