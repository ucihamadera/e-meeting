
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Rooms
			</small>
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
        		<button class="btn btn-success" onclick="add_room()"><i class="glyphicon glyphicon-plus"></i> Add Room</button>

				<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
						
							<th>No</th>
							<th>Name</th>
							<th>Area</th>
							<th>Floor</th>
							<th>Capacity</th>
							<th>Admin</th>
							<th>Photo</th>
							<?php if(is_permit('update', 'rooms') || is_permit('delete', 'rooms')) { ?>
							<th>-</th>
							<?php } ?>
						
						</tr>
					</thead>
					<tbody>
					</tbody>

					<tfoot>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Area</th>
							<th>Floor</th>
							<th>Capacity</th>
							<th>Admin</th>
							<th>Photo</th>
							<?php if(is_permit('update', 'rooms') || is_permit('delete', 'rooms')) { ?>
							<th>-</th>
							<?php } ?>

						</tr>
					</tfoot>
				</table>
  				
  				<!-- form input/edit -->
				<hr>

				<!-- Bootstrap modal -->
				<div class="modal fade" id="modal_form" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                <h3 class="modal-title">room Form</h3>
				            </div>
				            <div class="modal-body form">
				            	<?=form_open('#', array('id'=>'form', 'class'=>'form-horizontal') ); ?>
				                	<?=form_hidden($csrf); ?>
				                    <input type="hidden" value="" name="id"/> 
				                    <div class="form-body">
				                        <div class="form-group">
											<label for="username" class="col-md-3 control-label">Name</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="room_name">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Admin</label>
											<div class="col-md-9">
												<select name="admin_id" class="form-control">
													<option value="">--select admin--</option>
													<?php 
														foreach ($admins as $admin) 
														{
															echo '<option value="'.$admin->id.'"> '.$admin->username.' </option>';
														}

													 ?>
												</select>
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Area</label>
											<div class="col-md-9">
												<select name="area_id" class="form-control">
													<option value="">--select area--</option>
													<?php 
														foreach ($areas as $area) 
														{
															echo '<option value="'.$area->id.'"> '.$area->name.' </option>';
														}

													 ?>
												</select>
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Address</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="address">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Floor</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="floor">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Capacity</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="capacity">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Facility</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="facility">
												<span class="help-block"></span>
											</div>
										</div>
										  <div class="form-group" id="photo-preview">
				                            <label class="control-label col-md-3">Photo</label>
				                            <div class="col-md-9">
				                                (No photo)
				                                <span class="help-block"></span>
				                            </div>
				                        </div>
				                        <div class="form-group">
				                            <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
				                            <div class="col-md-9">
				                                <input name="photo" type="file">
				                                <span class="help-block"></span>
				                            </div>
				                        </div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Status</label>
											<div class="col-md-9">
												<select name="status" class="form-control" required="required">
													<option value="1">Active</option>
													<option value="0">Inactive</option>
												</select>
												<span class="help-block"></span>
											</div>
										</div>
										
				                    </div>
				                <?=form_close();?>
				            </div>
				            <div class="modal-footer">
				                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				            </div>
				        </div><!-- /.modal-content -->
				    </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<!-- End Bootstrap modal -->
  				

			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

  
       