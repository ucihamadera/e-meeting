
<div class="page-content" style="background:#FFFFF;">
	<div class="page-header">
		<h1>
			Home
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				Areas
			</small>
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
        		<button class="btn btn-success" onclick="add_area()"><i class="glyphicon glyphicon-plus"></i> Add Area</button>

				<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
						
							<th>No</th>
							<th>Name</th>
							<th>Description</th>
							<th>Status</th>
							<?php if(is_permit('update', 'areas') || is_permit('delete', 'areas')) { ?>
							<th>-</th>
							<?php } ?>
						
						</tr>
					</thead>
					<tbody>
					</tbody>

					<tfoot>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Description</th>
							<th>Status</th>
							<?php if(is_permit('update', 'areas') || is_permit('delete', 'areas')) { ?>
							<th>-</th>
							<?php } ?>

						</tr>
					</tfoot>
				</table>
  				
  				<!-- form input/edit -->
				<hr>

				<!-- Bootstrap modal -->
				<div class="modal fade" id="modal_form" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                <h3 class="modal-title">area Form</h3>
				            </div>
				            <div class="modal-body form">
				            	<?=form_open('#', array('id'=>'form', 'class'=>'form-horizontal') ); ?>
				                	<?=form_hidden($csrf); ?>
				                    <input type="hidden" value="" name="id"/> 
				                    <div class="form-body">
				                        <div class="form-group">
											<label for="username" class="col-md-3 control-label">Name</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="name">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Description</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="desc">
												<span class="help-block"></span>
											</div>
										</div>
										<div class="form-group">
											<label for="username" class="col-md-3 control-label">Status</label>
											<div class="col-md-9">
												<select name="status" class="form-control" required="required">
													<option value="active">Active</option>
													<option value="inactive">Inactive</option>
												</select>
												<span class="help-block"></span>
											</div>
										</div>
										
				                    </div>
				                <?=form_close();?>
				            </div>
				            <div class="modal-footer">
				                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				            </div>
				        </div><!-- /.modal-content -->
				    </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<!-- End Bootstrap modal -->
  				

			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

  
       