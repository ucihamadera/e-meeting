<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Areas extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation', 'encryption'));
		$this->load->helper(array('language', 'general'));

		$this->load->model('datatables_model','datatables');
		$this->load->model('general_model','general');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if (!$this->ion_auth->logged_in())
		{
			redirect('login-is-required', 'refresh');
		}
	}

	public function index()
	{
	
		if(is_permit('read', 'areas'))
		{
			$this->data['message'] 		= (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['csrf'] 		= $this->general->_get_csrf_nonce();
            $this->template
            ->set_partial('js', 'js_index')
            ->build('index', $this->data);
		}
		else
		{ 
			redirect('login-is-required', 'refresh');
		}
	
	}

    //ajax list area
    public function ajax_list_area()
    {

        $table 			= 'area';
		$column_order 	= array(null, 'name', 'desc', 'status');   
		$column_search 	= array('name', 'desc', 'status'); 
		$filters     	= array('name', 'desc', 'status');
		$order 			= array('name' => 'asc'); // default order 

        $list 	= $this->datatables->get_datatables($table, $column_order, $column_search, $order, $filters);
        $data 	= array();
        $update = '';
        $delete = '';
        $no = $_POST['start'];
        foreach ($list as $area) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $area->name;
            $row[] = $area->desc;
            $row[] = $area->status;

			if(is_permit('update', 'areas') || is_permit('delete', 'areas') )
			{
				if(is_permit('update', 'areas'))
				{
					$update = '<a class="green" href="#" onclick="edit_area(\''.$area->id.'\')">
									<i class="ace-icon fa fa-pencil bigger-130"></i>
								</a>';
				}
				
				if(is_permit('delete', 'areas'))
				{
					$delete = '<a class="red" href="#" onclick="delete_area(\''.$area->id.'\')">
									<i class="ace-icon fa fa-trash-o bigger-130"></i>
								</a>';
				}

	            $row[] = '	<div class="hidden-sm hidden-xs action-buttons">
								'.$update.$delete.'
							</div>';
			}

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatables->count_all(),
                        "recordsFiltered" => $this->datatables->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_add_area()
	{
		$data 	= array(
				'name' => $this->input->post('name'),
				'desc' => $this->input->post('desc'),
				'status' => $this->input->post('status'),
			);

		$insert = $this->general->save('area', $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_edit_area($id)
	{
		$data = $this->general->get_by_id('area', $id);
		echo json_encode($data);
	}

	public function ajax_update_area()
	{
		$data 	= array(
				'name' => $this->input->post('name'),
				'desc' => $this->input->post('desc'),
				'status' => $this->input->post('status'),
			);

		$this->general->update('area', array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_area($id)
	{
		$this->general->delete_by_id('area', $id);
		echo json_encode(array("status" => TRUE));
	}  

}

/* End of file Areas.php */
/* Location: ./application/modules/areas/controllers/Areas.php */