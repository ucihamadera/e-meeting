<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation', 'encryption'));
		$this->load->helper(array('language', 'general'));

		$this->load->model('datatables_model','datatables');
		$this->load->model('general_model','general');
		$this->load->model('booking_model','model');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if (!$this->ion_auth->logged_in())
		{
            // redirect them to the login page
			redirect('login-is-required', 'refresh');
		}
	}

	public function index()
	{
	
		if(is_permit('read', 'booking'))
		{
			$this->data['message'] 		= (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['csrf'] 		= $this->general->_get_csrf_nonce();
			$this->data['areas'] 		= $this->db->get('area')->result();
            $this->template
            ->set_partial('js', 'js_index')
            ->build('index', $this->data);
		}
		else
		{ 
			redirect('login-is-required', 'refresh');
		}
	
	}

	public function get_events()
	{
		$area = $this->input->get('area_id');
		$room = $this->input->get('room_id');
		// var_dump($_GET);exit;
		if(!empty($area) && !empty($room))
		{
			$data = $this->db->get_where('event', array('area_id' => $area, 'room_id' => $room, 'status' => 'active') )->result();
		}
		else
		{
			$data = array();
			// $data = $this->db->get('event')->result();
		}

		echo json_encode($data);
	}

	public function get_rooms()
	{
		$area = $this->input->post('area');
		$data = $this->db->get_where('room', array('area_id' => $area) )->result();
		$resp = '';
		foreach ($data as $value) 
		{
			$resp .= '<option value="'.$value->id.'">'.$value->room_name.' - floor '.$value->floor.'</option>';
		}

		echo $resp;

	}


    public function ajax_add_event()
	{
		$this->_validate();
		$admin_id 	= $this->db->get_where('room', array('id' => $this->input->post('room_id') ) )->row()->admin_id;
		$even_date 	= explode(' - ', $this->input->post('event_date') );
		$start 		= $even_date[0];
		$end 		= $even_date[1];
		$data 	= array(
				'date_created'	=> date('Y-m-d H:i:s'),
				'user_id'		=> $this->session->userdata('user_id'),
				'admin_id'		=> $admin_id,
				'area_id'		=> $this->input->post('area_id'),
				'room_id'		=> $this->input->post('room_id'),
				'title' 		=> $this->input->post('title'),
				'desc' 			=> $this->input->post('desc'),
				'notes' 		=> $this->input->post('notes'),
				'qty' 			=> $this->input->post('qty'),
				'start' 		=> $start,
				'end' 			=> $end,
				'status' 		=> $this->model->gen_status($this->input->post('room_id'), $this->session->userdata('user_id')),
			);

		$insert = $this->general->save('event', $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit_event($id)
	{
		$data = $this->general->get_by_id('event', $id);
		$data->event_date 	= $data->start.' - '.$data->end;
		$data->start_date 	= date("m/d/Y", strtotime($data->start));
		$data->end_date 	= date("m/d/Y", strtotime($data->end));
		$data->is_allow 	= $this->model->is_allow($id, $data->room_id, $this->session->userdata('user_id') );
		echo json_encode($data);
	}

	//ajax update group
	public function ajax_update_event()
	{
		$this->_validate();
		$admin_id 	= $this->db->get_where('room', array('id' => $this->input->post('room_id') ) )->row()->admin_id;
		$even_date 	= explode(' - ', $this->input->post('event_date') );
		$start 		= $even_date[0];
		$end 		= $even_date[1];
		$data 	= array(
				'date_created'	=> date('Y-m-d H:i:s'),
				'user_id'		=> $this->session->userdata('user_id'),
				'admin_id'		=> $admin_id,
				'area_id'		=> $this->input->post('area_id'),
				'room_id'		=> $this->input->post('room_id'),
				'title' 		=> $this->input->post('title'),
				'desc' 			=> $this->input->post('desc'),
				'notes' 		=> $this->input->post('notes'),
				'qty' 			=> $this->input->post('qty'),
				'start' 		=> $start,
				'end' 			=> $end,
			);

		$this->general->update('event', array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}
	//ajax delete group
	public function ajax_delete_event($id)
	{
		$this->general->delete_by_id('event', $id);
		echo json_encode(array("status" => TRUE));
	}  

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		$even_date 	= explode(' - ', $this->input->post('event_date') );
		$start 		= $even_date[0];
		if(!$this->model->is_duplicate_event($this->input->post('id'), $start, $this->input->post('area_id'), $this->input->post('room_id') ) )
		{
			$data['inputerror'][] = 'event_date';
			$data['error_string'][] = 'Event Date is already existed';
			$data['status'] = FALSE;
		}

		if($this->input->post('area_id') == '')
		{
			$data['inputerror'][] = 'area_id';
			$data['error_string'][] = 'Area is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('room_id') == '')
		{
			$data['inputerror'][] = 'room_id';
			$data['error_string'][] = 'Room is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('title') == '')
		{
			$data['inputerror'][] = 'title';
			$data['error_string'][] = 'Name is required';
			$data['status'] = FALSE;
		}


		if($this->input->post('event_date') == '')
		{
			$data['inputerror'][] = 'event_date';
			$data['error_string'][] = 'Event Date is required';
			$data['status'] = FALSE;
		}

		if ($this->general->_valid_csrf_nonce() === FALSE )
		{
			$data['inputerror'][] = 'msg';
			$data['error_string'][] = 'Go Home Kids | reload pages';
			$data['status'] = FALSE;
		}
			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}   

}

/* End of file Booking.php */
/* Location: ./application/modules/booking/controllers/Booking.php */